package no.uib.info233.v2016.skn003.oblig2.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import no.uib.info233.v2016.skn003.oblig2.issue.Issue;
import no.uib.info233.v2016.skn003.oblig2.issue.IssueTracker;

/**
 * 
 * The GUI class represents the gui of the program
 * �t contains methods for initialising the gui and creating a MenuBar
 * @author Skn003
 *
 */
public class GUI extends JFrame {

	// Fields
	private static final long serialVersionUID = -7751201936038620525L;
	
	public static GUI gui = new GUI();
	private IssueTracker it;
	
	private JTable issueTable;
	Object columnNames[] = {
			"ID", "Assigned User", "Created", "Text", "Priority", "Location"
	};
	
	/**
	 * Initialises the GUI
	 */
	public void initGUI(IssueTracker newIt) {
		
		this.it = newIt;
		
		it.readFile();
		
		createMenuBar();
		
		setTitle("Oblig2");
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		try {
			issueTable = new JTable(it.getData(), columnNames);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JScrollPane scrollPane = new JScrollPane(issueTable);
		add(scrollPane, BorderLayout.CENTER);
		add(issueTable);
		
		pack();
		
	}
	
	/**
	 * Creates a MenuBar and five options for the "File" menu
	 */
	private void createMenuBar() {

        JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("File");

        JMenuItem read = new JMenuItem("Read file");
        read.setToolTipText("Read XML file");
        read.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				try {
					it.readFile();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
        	
        });
        JMenuItem write = new JMenuItem("Write file");
        write.setToolTipText("Write to XML file");
        write.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				it.writeToFile();
				
			}
		});
        JMenuItem priority = new JMenuItem("Highest priority");
        write.setToolTipText("Get issue with highest priority");
        write.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Issue issue = null;
				it.getHighPrioIssue(issue);
				
			}
		});
        JMenuItem created = new JMenuItem("From timeframe");
        write.setToolTipText("Get issue from a specified timeframe");
        write.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				it.getIssueByTime();
				
			}
		});
        JMenuItem addIssue = new JMenuItem("Add issue");
        addIssue.setToolTipText("Add issue to table");
        addIssue.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				
				
			}
		});
        JMenuItem addEmployee = new JMenuItem("Add employee");
        addEmployee.setToolTipText("Add employee to table");
        addEmployee.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				
				
			}
		});
        JMenuItem exit = new JMenuItem("Exit");
        exit.setToolTipText("Exit application");
        exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				
				System.exit(0);
				
			}
        	
        });

        file.add(read);
        file.add(write);
        file.add(priority);
        file.add(created);
        file.add(addIssue);
        file.add(addEmployee);
        file.add(exit);
        menubar.add(file);

        setJMenuBar(menubar);
    }

	/**
	 * @return the columnNames
	 */
	public Object[] getColumnNames() {
		return columnNames;
	}

	/**
	 * @param columnNames the columnNames to set
	 */
	public void setColumnNames(Object[] columnNames) {
		this.columnNames = columnNames;
	}

	/**
	 * @return the issueTable
	 */
	public JTable getIssueTable() {
		return issueTable;
	}

	/**
	 * @param issueTable the issueTable to set
	 */
	public void setIssueTable(JTable table) {
		this.issueTable = table;
	}

	/**
	 * @return the gui
	 */
	public static GUI getGui() {
		return gui;
	}

	/**
	 * Sets/overwrites the GUI singleton. Don't use this.
	 * 
	 * @param gui the gui to set
	 */
	public static void setGui(GUI gui) {
		GUI.gui = gui;
	}
	
}
