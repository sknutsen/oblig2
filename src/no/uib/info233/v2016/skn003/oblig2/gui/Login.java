package no.uib.info233.v2016.skn003.oblig2.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.uib.info233.v2016.skn003.oblig2.issue.IssueTracker;

/**
 * 
 * The Login class represents the login window
 * It contains a method to initialise it and the main GUI when a username and password is entered correctly
 * @author skn003
 *
 */
public class Login extends JDialog{
	
	// Fields
	private static final long serialVersionUID = 1L;
	
	public GUI gui;
	
	private JTextField username;
	private JTextField password;
	
	private JButton login;
	private JLabel statusBar;
	private JPanel panel;
	
	/**
	 * Constructor for the Login class
	 * 
	 * @param parent the parent GUI
	 */
	public Login(GUI parent, IssueTracker it) {
		super(parent);
		
		this.gui = parent;
		
		initUI(it);
		
	}
	
	/**
	 * Initialises the Login GUI with two JTextFields, a JButton and a JLabel at the bottom of the window
	 */
	private void initUI(final IssueTracker it) {
		
		setTitle("Login");
		setSize(300, 100);
		setLocationRelativeTo(null);
		
		statusBar = new JLabel("Enter Username/Password and click OK.");
		statusBar.setBorder(BorderFactory.createEtchedBorder());
		add(statusBar, BorderLayout.SOUTH);
		
		username = new JTextField("Username");
		password = new JTextField("Password");
		login = new JButton("OK");
		login.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String user = "Username";
				String pass = "Password";
				
					if ((user.equals(username.getText())) && (pass.equals(password.getText()))) {
					
							gui.initGUI(it);
							gui.setVisible(true);
							dispose();
						
					} else {
					
						statusBar.setText("Wrong Username or Password!");
					
					}
				
				}
				
		});
		panel = new JPanel();
		
		panel.add(username);
		panel.add(password);
		panel.add(login);
		
		add(panel);
		
	}

	/**
	 * @return the statusBar
	 */
	public JLabel getStatusBar() {
		return statusBar;
	}

	/**
	 * @param statusBar the statusBar to set
	 */
	public void setStatusBar(JLabel statusBar) {
		this.statusBar = statusBar;
	}

	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * @param panel the panel to set
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	/**
	 * @return the username
	 */
	public JTextField getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(JTextField username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public JTextField getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(JTextField password) {
		this.password = password;
	}

	/**
	 * @return the login
	 */
	public JButton getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(JButton login) {
		this.login = login;
	}
	
}
