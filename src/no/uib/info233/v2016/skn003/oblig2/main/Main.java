package no.uib.info233.v2016.skn003.oblig2.main;

import java.awt.EventQueue;
import no.uib.info233.v2016.skn003.oblig2.gui.GUI;
import no.uib.info233.v2016.skn003.oblig2.gui.Login;
import no.uib.info233.v2016.skn003.oblig2.issue.IssueTracker;

/**
 * 
 * The Main class that starts the program
 * @author skn003
 *
 */
public class Main {
	
	/**
	 * main method for the program
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
	        
	           @Override
	           public void run() {
	        	   
	        	   GUI gui = GUI.getGui();
	        	   
	        	   IssueTracker it = new IssueTracker();
	        	   
	        	   Login login = new Login(gui, it);
	        	   login.setVisible(true);
	               
	          }
	           
		});
		
	}
	
}
