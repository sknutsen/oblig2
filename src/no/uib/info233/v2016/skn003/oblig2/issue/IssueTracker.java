package no.uib.info233.v2016.skn003.oblig2.issue;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import no.uib.info233.v2016.skn003.oblig2.gui.GUI;

/**
 * 
 * The IssueTracker class handles Issues and Employees
 * It contains methods for reading and writing to files and also creating new Issue and Employee objects
 * @author skn003
 *
 */
public class IssueTracker {

	// Fields
	private ArrayList<Issue> issues;
	private Object[][] data = {
		
		{issues}
		
	};
	private ArrayList<Employee> employees;
	
	/**
	 * Constructor for the IssueTracker class
	 * 
	 * @param issues an ArrayList containing issues
	 * @param gui the GUI the IssueTracker uses
	 */
	public IssueTracker() {
		
		issues = new ArrayList<>();
		employees = new ArrayList<>();
		
		readFile();
		
	}
	
	/**
	 * Reads a file and adds the elements to ArrayLists
	 */
	public void readFile() {
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document document = builder.parse(new File("old_issues.xml"));
			
			NodeList nodeList = document.getElementsByTagName("ISSUES");
			
			for(int x = 0, size = nodeList.getLength(); x < size; x++) {
				
				String id = nodeList.item(x).getAttributes().getNamedItem("id").getNodeValue();
				String user = nodeList.item(x).getAttributes().getNamedItem("assigned_user").getNodeValue();
				String created = nodeList.item(x).getAttributes().getNamedItem("created").getNodeValue();
				String text = nodeList.item(x).getAttributes().getNamedItem("text").getNodeValue();
				String prio = nodeList.item(x).getAttributes().getNamedItem("priority").getNodeValue();
				String loc = nodeList.item(x).getAttributes().getNamedItem("location").getNodeValue();
				
				addIssue(id, user, created, text, prio, loc);
				
				addEmployee(user, loc);
				
			}
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Writes information about Issues and Employees to a file
	 */
	@SuppressWarnings("resource")
	public void writeToFile() {
		
		if(this.issues.size() == 0) {
			System.out.println("No issues");
			return;
		}
		
		String nl = "\n";
		String c = " ";
		
		try {
			FileWriter fw = new FileWriter("issues.xml");
			
			for(Issue s : issues) {
			
			fw.append("<ISSUES");
			fw.append(c);
			fw.append("id=");
			fw.append(String.valueOf(s.getId()));
			fw.append(c);
			fw.append("assigned_user=");
			fw.append(String.valueOf(s.getUser()));
			fw.append(c);
			fw.append("created=");
			fw.append(String.valueOf(s.getCreated()));
			fw.append(c);
			fw.append("text=");
			fw.append(String.valueOf(s.getText()));
			fw.append(c);
			fw.append("priority=");
			fw.append(String.valueOf(s.getPriority()));
			fw.append(c);
			fw.append("location=");
			fw.append(String.valueOf(s.getLocation()));
			fw.append("/>");
			fw.append(nl);
				
			}
			
			FileWriter fw2 = new FileWriter("employees.xml");
			
			for(Employee e : employees) {
				
				fw2.append("<EMPLOYEES");
				fw2.append(c);
				fw2.append("username=");
				fw2.append(String.valueOf(e.getUsername()));
				fw2.append(c);
				fw2.append("location=");
				fw2.append(String.valueOf(e.getLocation()));
				fw2.append("/>");
				fw2.append(nl);
				
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Gets the Issue with the highest priority
	 * 
	 * @return
	 */
	public Issue getHighPrioIssue(Issue i) {
		
		if(issues.size() <= 0) return null;
		
		for(Issue issue : issues) {
			
			int x = Integer.parseInt(issue.getPriority());
			int y = Integer.parseInt(issue.getPriority());
			
			if(y > x) {
				
				return getHighPrioIssue(issue);
				
			} else {
				
				System.out.println(i.getId() + " " + i.getUser() + " " + i.getCreated() + " " + i.getText() + " " + i.getPriority() + " " + i.getLocation());
				return i;
				
			}
			
		}
		
		return i;
		
	}
	
	/**
	 * gets the issues created between specified dates
	 * 
	 * @return issue the issue from after the given date
	 */
	public void getIssueByTime() {
		
		final JFrame frame = new JFrame("dateSearch");
		frame.setSize(300, 100);
		frame.setLocationRelativeTo(null);
		
		final JLabel statusBar = new JLabel("Enter dates");
		statusBar.setBorder(BorderFactory.createEtchedBorder());
		frame.add(statusBar, BorderLayout.SOUTH);
		
		final JTextField startDate = new JTextField("Latest");
		final JTextField stopDate = new JTextField("Earliest");
		JButton enter = new JButton("OK");
		enter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
					if (startDate.getText() != null && stopDate.getText() != null) {
						
						ArrayList<Issue> il = new ArrayList<Issue>();
						
						int firstDate = Integer.parseInt(startDate.getText());
						int lastDate = Integer.parseInt(stopDate.getText());
						
						for(Issue issue : issues) {
							
							if(Integer.parseInt(issue.getCreated()) < firstDate && Integer.parseInt(issue.getCreated()) > lastDate) {
								
								il.add(issue);
								
							}
							
						}
						
						Object[][] ilData = {{il}};
						GUI gui = GUI.getGui();
						
						JTable issueTable = new JTable(ilData, gui.getColumnNames());
						
						JScrollPane scrollPane = new JScrollPane(issueTable);
						frame.add(scrollPane, BorderLayout.CENTER);
						frame.add(issueTable);
					
					} else {
					
						statusBar.setText("Invalid input!");
					
					}
				
				}
				
		});
		JPanel panel = new JPanel();
		
		panel.add(startDate);
		panel.add(stopDate);
		panel.add(enter);
		
		frame.add(panel);
		
		frame.setVisible(true);
		
	}
	
	/**
	 * Adds a new Issue to the issues ArrayList
	 * 
	 * @param id the id of the Issue
	 * @param user the assigned_user of the Issue
	 * @param created when the Issue was created
	 * @param text the text of the Issue
	 * @param priority the priority of the Issue
	 * @param location the location of the assigned_user
	 * @return boolean
	 */
	public boolean addIssue(String id, String user, String created, String text, String priority, String location) {
		
		Issue issue = new Issue(id, user, created, text, priority, location);
		
		if(this.issues.contains(issue)) {
			
			return false;
			
		} else {
			
			this.issues.add(issue);
			return true;
			
		}
		
	}
	
	/**
	 * Removes an Issue
	 * 
	 * @param issue the Issue to be removed
	 * @return
	 */
	public boolean removeIssue(Issue issue) {
		
		if(this.issues.contains(issue)) {
			
			this.issues.remove(issue);
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Clears all Issues from the issues ArrayList
	 * 
	 * @return boolean
	 */
	public boolean clearIssues() {
		
		if(this.issues.size() == 0) return false;
		
		this.issues.clear();
		return true;
		
	}
	
	/**
	 * Adds a new Employee to the employees ArrayList
	 * 
	 * @param user the assigned_user of the Issue
	 * @param location the location of the assigned_user
	 * @return boolean
	 */
	public boolean addEmployee(String user, String location) {
		
		Employee employee = new Employee(user, location);
		
		if(this.employees.contains(employee)) {
			
			return false;
			
		} else {
			
			this.employees.add(employee);
			return true;
			
		}
		
	}
	
	/**
	 * Removes an Employee
	 * 
	 * @param employee the Employee to be removed
	 * @return
	 */
	public boolean removeEmployee(Employee employee) {
		
		if(this.employees.contains(employee)) {
			
			this.employees.remove(employee);
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Clears all Employees from the employees ArrayList
	 * 
	 * @return boolean
	 */
	public boolean clearEmplopyees() {
		
		if(this.employees.size() == 0) return false;
		
		this.employees.clear();
		return true;
		
	}

	/**
	 * @return the data
	 */
	public Object[][] getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object[][] data) {
		this.data = data;
	}

	/**
	 * @return the employees
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	/**
	 * @param employees the employees to set
	 */
	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}

	/**
	 * @return the issues
	 */
	public ArrayList<Issue> getIssues() {
		return issues;
	}

	/**
	 * @param issues the issues to set
	 */
	public void setIssues(ArrayList<Issue> issues) {
		this.issues = issues;
	}
	
}
