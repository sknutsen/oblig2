package no.uib.info233.v2016.skn003.oblig2.issue;

/**
 * 
 * The Employee class represents an employee
 * @author skn003
 *
 */
public class Employee {
	
	// Fields
	private String username;
	private String location;
	
	/**
	 * @param username
	 * @param location
	 */
	public Employee(String username, String location) {
		
		setUsername(username);
		setLocation(location);
		
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

}
