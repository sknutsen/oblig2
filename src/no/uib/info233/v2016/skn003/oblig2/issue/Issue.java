package no.uib.info233.v2016.skn003.oblig2.issue;

/**
 * 
 * The Issue class represents an Issue
 * @author skn003
 *
 */
public class Issue {

	// Fields
	private String id;
	private String user;
	private String created;
	private String text;
	private String priority;
	private String location;
	
	/**
	 * @param id
	 * @param user
	 * @param created
	 * @param text
	 * @param priority
	 * @param location
	 */
	public Issue(String id, String user, String created, String text, String priority, String location) {
		
		setId(id);
		setUser(user);
		setCreated(created);
		setText(text);
		setPriority(priority);
		setLocation(location);
		
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the created
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	
}
